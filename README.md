# Experimental images with OpenTofu to support its GitLab integrations.

For the work needed for OpenTofu support, please go to https://gitlab.com/gitlab-org/terraform-images/-/issues/114

## Description

The images in this repo are built with `nix`. 

### Adding support for a new version

1. [Search nix packages](https://search.nixos.org/packages?channel=unstable&from=0&size=50&sort=relevance&type=packages&query=opentofu) and not the opentofu version
1. [Look up the related sha for the nixpkgs version](https://status.nixos.org/)
1. [Extend the matrix build](./.gitlab-ci.yml) with the sha and version
