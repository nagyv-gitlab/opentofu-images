{ pkgs ? import <nixpkgs> { }
  , pkgsLinux ? import <nixpkgs> { system = "x86_64-linux"; }
}:

pkgs.dockerTools.buildImage {
  name = "gitlab-terraform-kubeseal-cosign";
  copyToRoot = pkgs.buildEnv {
    name = "image-root";
    paths = [
      pkgs.bash
      pkgs.gnugrep
      pkgs.coreutils
      pkgs.gawk
      pkgs.gnused
      pkgs.libidn2
      pkgs.jq
      pkgs.opentofu
      (pkgs.writeScriptBin "gitlab-terraform" (builtins.readFile (pkgs.fetchurl {
        url="https://gitlab.com/gitlab-org/terraform-images/-/raw/v1.5.1/src/bin/gitlab-terraform.sh";
        hash="sha256-F8UwXdh9ZntPcaPrfu7RtNun657TYC2OT8UKaqb8aPY=";
     })))
    ];
    pathsToLink = [ "/bin" ];
  };

  config = {
    #WorkingDir = "/app";
    Volumes = { "/tmp" = { }; };
    Env = [ "SSL_CERT_FILE=${pkgs.cacert}/etc/ssl/certs/ca-bundle.crt" ];
  };
}