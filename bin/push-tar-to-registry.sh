#!/bin/bash
# Push the built image to the container registry

## Crane does not support gzipped containers 
gzip --decompress --stdout --force < $1 > $1.tar
crane auth login -u "${CI_REGISTRY_USER}" -p "${CI_REGISTRY_PASSWORD}" "${CI_REGISTRY}"
crane push $1.tar "${CI_REGISTRY_IMAGE}/$BUILD_IMAGE_NAME:${IMAGE_TAG}"
